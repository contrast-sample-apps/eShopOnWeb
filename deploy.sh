#!/bin/sh
echo "Move stuff around"
sudo chown -R ubuntu:ubuntu /home/ubuntu/webapps/eShopOnWeb
sudo rm -rf /home/ubuntu/webapps/eShopOnWeb.tar.gz
echo "Start containers"
cd /home/ubuntu/webapps/eShopOnWeb/
sudo docker-compose up -d
sleep 25
echo "Curl URLS"
sudo curl --silent --output /dev/null -X GET "http://eshoponweb.contrast.pw:5106/"
sudo curl --silent --output /dev/null -X GET "http://eshoponweb.contrast.pw:5106/Basket/"
sudo curl --silent --output /dev/null -X GET "http://eshoponweb.contrast.pw:5106/?CatalogModel.BrandFilterApplied=5&CatalogModel.TypesFilterApplied=1&x=14&y=13"
sudo curl --silent --output /dev/null -X GET "http://eshoponweb.contrast.pw:5106/"
sleep 10
