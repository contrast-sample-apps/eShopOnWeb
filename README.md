# Microsoft eShopOnWeb ASP.NET Core application with Contrast
Sample ASP.NET Core reference application, powered by Microsoft, demonstrating a single-process (monolithic) application architecture and deployment model. 

## Contrast Instrumentation 
This repo includes the components necessary to instrument contrast Assess/Protect with this dotnet Core application except for the contrast_security.yaml file containing the connection strings.

Specifically modified:

1. WebGoatCore.csproj includes the Contrast.SensorsNetCore NuGet package as a dependency.
2. The docker-compose.yml includes the path to the contrast_security.yaml (not included), dotnet Core specific environment variables required (CORECLR_PROFILER_PATH_64, CORECLR_PROFILER, and CORECLR_ENABLE_PROFILING=1), and a few other specific environment variables.
3. Three other docker-compose YAMLs depending on what "environment" you're wanting to run: Development, QA, or Production.

contrast_security.yaml example:

api:<br>
&nbsp;&nbsp;url: https://apptwo.contrastsecurity.com/Contrast<br>
&nbsp;&nbsp;api_key: [REDACTED<br>
&nbsp;&nbsp;service_key: [REDACTED]<br>
&nbsp;&nbsp;user_name: [REDACTED]<br>
application:<br>
&nbsp;&nbsp;session_metadata: buildNumber=${BUILD_NUMBER}, committer=Steve Smith #buildNumber is inserted via Jenkins Pipeline<br>

Your contrast_security.yaml file needs to be in the root of the web application directory. It then gets copied into the Docker Container.

# Requirements

1. Docker Community Edition
2. docker-compose

When built, the Dockerfile pulls in all of the source code and builds the dotnet Core application. 

## How to build and run

### 1. Running in a Docker Container

The provided Dockerfile is compatible with both Linux and Windows containers (note from Steve: I've only run it on Linux).

To build a Docker image, execute the following command: docker-compose build

### Linux Containers

To run the `eshoponewebmvc` Container image, execute one of the following commands:

1. Development: docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d

2. QA: docker-compose -f docker-compose.yml -f docker-compose.qa.yml up -d

3. Production (this disables Assess and enables Protect): docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

Nodegoat should be accessible at http://ip_address:5106.


### Stopping the Docker container

To stop the `eshoponwebmvc` container, execute the following command in the same directory as your docker-compose files: docker-compose stop 

### 2. Building with Jenkins
Included is a sample Jenkinsfile that can be used as a Jenkins Pipeline to build and run the application. The Jenkins Pipeline passes buildNumber as a parameter to the YAML. 

#### Default user accounts
The database comes pre-populated with these user accounts created as part of the seed data -
* Admin Account - u:admin@microsoft.com p:Pass@word1 
* User Accounts - u:demouser@microsoft.com p:Pass@word1
* New users can also be added using the sign-up page.

